import { Utilisateur } from './Utilisateur';
import { Conversation } from './Conversation';

export class Message {
    public id: number;
    public contenu: string;
    public date_cre: Date;
    public date_mod: Date;
    public flag: boolean;
    public emetteur: Utilisateur;
    public conversation: Conversation;

    public constructor() { }
}