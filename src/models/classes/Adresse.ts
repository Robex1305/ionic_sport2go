export class Adresse {
    public id: number;
    public adresse: string;
    public ville: string;
    public codePostal: string;
    public pays: string;
    public date_cre: Date;
    public date_mod: Date;
    public flag: boolean;

    public constructor() { }
}
