import { Utilisateur } from './Utilisateur';
import { Conversation } from './Conversation';

export class Groupe {
    public id: number;
    public nom: string;
    public description: string;
    public date_cre: Date;
    public date_mod: Date;
    public flag: boolean;
    public image: string;
    public conversation: Conversation;
    public membres: Array<Utilisateur>;
    public admin: Utilisateur;

    public constructor() {
        this.conversation = new Conversation();
        this.conversation.titre = this.nom;
        this.conversation.participants = this.membres;
    }
}