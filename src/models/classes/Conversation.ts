import { Message } from './Message';
import { Utilisateur } from './Utilisateur';
import { UrlResolver } from '@angular/compiler';

export class Conversation {
    public id: number;
    public titre: string;
    public date_cre: Date;
    public date_mod: Date;
    public flag: boolean;
    public typeGroupe: boolean;
    public participants: Array<Utilisateur>;

    constructor() {
        this.participants = new Array<Utilisateur>();
    }

    public addMembre(utilisateur: Utilisateur) {
        this.participants.push(utilisateur);
    }
}