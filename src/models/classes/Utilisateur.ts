import { Evenement } from "./Evenement";
import { Sport } from './Sport';

export class Utilisateur {
    public id: number;
    public nom: string;
    public prenom: string;
    public dateNaissance: Date;
    public date_cre: Date;
    public date_mod: Date;
    public email: string;
    public flag: boolean;
    public password: string;
    public roles: Array<String>;
    public sports: Array<Sport>;

    public constructor() {
        this.sports = new Array<Sport>();
    }

    public getNomComplet() {
        return this.prenom + " " + this.nom;
    }
}

