export class Geolocalisation {
    public id: number;
    public libelle: string;
    public latitude: number;
    public longitude: number;
    public date_cre: Date;
    public date_mod: Date;
    public flag: boolean;

    public constructor() { }
}