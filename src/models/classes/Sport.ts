export class Sport {
    public id: number;
    public nom: string;
    public image: string;
    public description: string;
    public date_cre: Date;
    public date_mod: Date;
    public flag: boolean;

    public constructor() { }
}