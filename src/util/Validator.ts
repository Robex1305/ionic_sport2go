import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class Validator{

    public constructor(){

    }
    
    public validateEmail(mail) {
      let regex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      if (regex.test(mail)) {
        return (true)
      }
      else{
        return false;
      }
    }
}