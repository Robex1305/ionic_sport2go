import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Adresse } from 'src/models/classes/Adresse';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SessionManager } from 'src/util/SessionManager';

@Injectable({
    providedIn: 'root'
})

export class AdresseService {
    public baseURL;
    public constructor(private httpClient: HttpClient, public events: Events, public sessionManager: SessionManager) {
        this.baseURL = environment.urlAPI + "/adresses"
    }

    public create(adresse: Adresse) {
        const url = this.baseURL + "/create";
        let token = this.sessionManager.get("token");

        console.log(JSON.stringify(adresse));
        return this.httpClient.post<Adresse>(url, adresse, {
            headers: { "Authorization": token }
        });
    }

    public read(id: number) {
        const url = this.baseURL + "/read?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Adresse>(url, {
            headers: { "Authorization": token }
        })
    }

    public delete(id: number) {
        const url = this.baseURL + "/delete?id=" + id;
        let token = this.sessionManager.get("token");

        return this.httpClient.delete<Response>(url, {
            headers: { "Authorization": token }
        });
    }

    public readAll() {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Adresse[]>(this.baseURL + "/readAll", {
            headers: { "Authorization": token }
        });
    }
}