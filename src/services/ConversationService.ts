import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Conversation } from 'src/models/classes/Conversation';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SessionManager } from 'src/util/SessionManager';

@Injectable({
    providedIn: 'root'
})

export class ConversationService {
    public baseURL;
    public constructor(private httpClient: HttpClient, public events: Events, public sessionManager: SessionManager) {
        this.baseURL = environment.urlAPI + "/conversations"
    }

    public create(conversation: Conversation) {
        const url = this.baseURL + "/create";
        let token = this.sessionManager.get("token");
        // console.log(JSON.stringify(conversation));
        return this.httpClient.post<Conversation>(url, conversation, {
            headers: { "Authorization": token }
        });
    }

    public read(id: number) {
        const url = this.baseURL + "/read?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Conversation>(url, {
            headers: { "Authorization": token }
        })
    }

    public delete(id: number) {
        const url = this.baseURL + "/delete?id=" + id;
        let token = this.sessionManager.get("token");

        return this.httpClient.delete<Response>(url, {
            headers: { "Authorization": token }
        });
    }

    public readAll() {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Conversation[]>(this.baseURL + "/readAll", {
            headers: { "Authorization": token }
        });
    }
}