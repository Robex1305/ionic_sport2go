import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Geolocalisation } from 'src/models/classes/Geolocalisation';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SessionManager } from 'src/util/SessionManager';

@Injectable({
    providedIn: 'root'
})

export class GeolocalisationService {
    public baseURL;
    public constructor(private httpClient: HttpClient, public events: Events, public sessionManager: SessionManager) {
        this.baseURL = environment.urlAPI + "/geolocalisations"
    }

    public create(geolocalisation: Geolocalisation) {
        const url = this.baseURL + "/create";
        let token = this.sessionManager.get("token");

        console.log(JSON.stringify(geolocalisation));
        return this.httpClient.post<Geolocalisation>(url, geolocalisation, {
            headers: { "Authorization": token }
        });
    }

    public read(id: number) {
        const url = this.baseURL + "/read?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Geolocalisation>(url, {
            headers: { "Authorization": token }
        })
    }

    public delete(id: number) {
        const url = this.baseURL + "/delete?id=" + id;
        let token = this.sessionManager.get("token");

        return this.httpClient.delete<Response>(url, {
            headers: { "Authorization": token }
        });
    }

    public readAll() {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Geolocalisation[]>(this.baseURL + "/readAll", {
            headers: { "Authorization": token }
        });
    }
}