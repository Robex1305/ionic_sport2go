import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Sport } from 'src/models/classes/Sport';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SessionManager } from 'src/util/SessionManager';

@Injectable({
    providedIn: 'root'
})

export class SportService {
    public baseURL;
    public constructor(private httpClient: HttpClient, public events: Events, public sessionManager: SessionManager) {
        this.baseURL = environment.urlAPI + "/sports"
    }

    public create(sport: Sport) {
        const url = this.baseURL + "/create";
        let token = this.sessionManager.get("token");

        console.log(JSON.stringify(sport));
        return this.httpClient.post<Sport>(url, sport, {
            headers: { "Authorization": token }
        });
    }

    public read(id: number) {
        const url = this.baseURL + "/read?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Sport>(url, {
            headers: { "Authorization": token }
        })
    }

    public delete(id: number) {
        const url = this.baseURL + "/delete?id=" + id;
        let token = this.sessionManager.get("token");

        return this.httpClient.delete<Response>(url, {
            headers: { "Authorization": token }
        });
    }

    public readAll() {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Sport[]>(this.baseURL + "/readAll", {
            headers: { "Authorization": token }
        });
    }
}