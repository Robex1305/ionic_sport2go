import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SessionManager } from 'src/util/SessionManager';
import { Utilisateur } from 'src/models/classes/Utilisateur';


@Injectable({
    providedIn: 'root'
})

export class UtilisateurService {
    public baseURL;
    public constructor(private httpClient: HttpClient, public sessionManager: SessionManager) {
        this.baseURL = environment.urlAPI + "/users"
    }

    public readAll() {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Utilisateur[]>(this.baseURL + "/readAll", {
            headers: { "Authorization": token }
        });
    }

    public read(id: number) {
        const url = this.baseURL + "/read?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.get(url, {
            headers: { "Authorization": token }
        })
    }

    public delete(id: number) {
        const url = this.baseURL + "/delete?id=" + id;
        let token = this.sessionManager.get("token");

        return this.httpClient.delete(url, {
            headers: { "Authorization": token }
        });
    }

    public getCurrentUser() {
        const url = this.baseURL + "/currentUser";
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Utilisateur>(url, {
            headers: { "Authorization": token }
        });
    }

    public adminAccess() {
        const url = this.baseURL + "/adminAccess";
        let token = this.sessionManager.get("token");
        return this.httpClient.get(url, {
            headers: { "Authorization": token }
        });
    }

}