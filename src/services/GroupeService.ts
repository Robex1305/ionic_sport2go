import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Groupe } from 'src/models/classes/Groupe';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SessionManager } from 'src/util/SessionManager';

@Injectable({
    providedIn: 'root'
})

export class GroupeService {
    public baseURL;
    public constructor(private httpClient: HttpClient, public events: Events, public sessionManager: SessionManager) {
        this.baseURL = environment.urlAPI + "/groupes"
    }

    public create(groupe: Groupe) {
        const url = this.baseURL + "/create";
        let token = this.sessionManager.get("token");
        // console.log(JSON.stringify(groupe));
        return this.httpClient.post<Groupe>(url, groupe, {
            headers: { "Authorization": token }
        });
    }

    public read(id: number) {
        const url = this.baseURL + "/read?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Groupe>(url, {
            headers: { "Authorization": token }
        })
    }

    public delete(id: number) {
        const url = this.baseURL + "/delete?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.delete<Response>(url, {
            headers: { "Authorization": token }
        });
    }

    public readAll() {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Groupe[]>(this.baseURL + "/readAll", {
            headers: { "Authorization": token }
        });
    }

    public addMembre(userId, groupeId) {
        const url = this.baseURL + "/addMembre";
        let token = this.sessionManager.get("token");
        let parametres = {
            "userId": userId,
            "groupeId": groupeId
        };

        return this.httpClient.post<Groupe[]>(url, parametres, {
            headers: { "Authorization": token }
        });
    }

    public removeMembre(userId, groupeId) {
        const url = this.baseURL + "/removeMembre";
        let token = this.sessionManager.get("token");
        let parametres = {
            "userId": userId,
            "groupeId": groupeId
        };
        return this.httpClient.post<Groupe[]>(url, parametres, {
            headers: { "Authorization": token }
        });
    }
}