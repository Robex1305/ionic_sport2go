import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from 'src/models/classes/Message';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { SessionManager } from 'src/util/SessionManager';

@Injectable({
    providedIn: 'root'
})

export class MessageService {
    public baseURL;
    public constructor(private httpClient: HttpClient, public events: Events, public sessionManager: SessionManager) {
        this.baseURL = environment.urlAPI + "/messages"
    }

    public create(message: Message) {
        const url = this.baseURL + "/create";
        let token = this.sessionManager.get("token");
        console.log(JSON.stringify(message));
        return this.httpClient.post<Message>(url, message, {
            headers: { "Authorization": token }
        });
    }

    public read(id: number) {
        const url = this.baseURL + "/read?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Message>(url, {
            headers: { "Authorization": token }
        })
    }

    public delete(id: number) {
        const url = this.baseURL + "/delete?id=" + id;
        let token = this.sessionManager.get("token");
        return this.httpClient.delete<Response>(url, {
            headers: { "Authorization": token }
        });
    }

    public readAll() {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Message[]>(this.baseURL + "/readAll", {
            headers: { "Authorization": token }
        });
    }

    public readAllByConversation(id: number) {
        let token = this.sessionManager.get("token");
        return this.httpClient.get<Message[]>(this.baseURL + "/readAllByConv?id=" + id, {
            headers: { "Authorization": token }
        });
    }
}