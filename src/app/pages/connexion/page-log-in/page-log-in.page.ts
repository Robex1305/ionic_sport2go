import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/services/AuthenticationService';
import { Router } from '@angular/router';
import { Popup } from 'src/util/Popup';
import { Utilisateur } from 'src/models/classes/Utilisateur';
import { UtilisateurService } from 'src/services/UtilisateurService';
import { Guardian } from 'src/util/Guardian';
import { createHash } from 'crypto';
import { SessionManager } from 'src/util/SessionManager';
import { Validator } from 'src/util/Validator';

@Component({
  selector: 'app-page-log-in',
  templateUrl: './page-log-in.page.html',
  styleUrls: ['./page-log-in.page.scss']
})
export class PageLogInPage implements OnInit {

  public username: string;
  public password: string;

  public constructor(public validator: Validator, public authService: AuthenticationService, public utilisateurService: UtilisateurService, public router: Router, public sessionManager: SessionManager, public popup: Popup) { }

  ngOnInit() {
    this.authService.checkToken().subscribe(res => {
      //Deja connecté, on redirige
      this.router.navigateByUrl("home");
    }, err => {
      //Pas connecté, on ne redirige pas.
    })
  }

  public login() {
    if (!this.username) {
      this.popup.showMessage("Veuillez saisir votre adresse email");
    }
    else if (this.validator.validateEmail(this.username)) {
      this.popup.showMessage("L'adresse email saisie est invalide");
    }
    else if (!this.password) {
      this.popup.showMessage("Veuillez saisir votre mot de passe");
    }
    else {
      this.popup.showLoaderCustom("Connexion...");
      this.authService.checkCredential(this.username, this.password).subscribe((user) => {
        this.authService.getToken(this.username, this.password).subscribe(data => {
          let token = "Bearer " + data['token'];
          this.utilisateurService.getCurrentUser().subscribe(user => {
            this.sessionManager.store('user', JSON.stringify(user));
          });
          this.sessionManager.store("token", token);
          this.router.navigateByUrl("home", {
            replaceUrl: true
          });
        })
        this.popup.hideLoader();
      },
        (error) => {
          if (error.status == 401) {
            this.popup.showMessage("Identifiant/mot de passe incorrecte(s)");
          } else {
            this.popup.showMessage("Une erreur est survenue, veuillez réessayer plus tard")
          }
          this.sessionManager.destroy();
          this.popup.hideLoader();
        });
    }
  }

  public register() {
    this.router.navigateByUrl("register", {
      replaceUrl: true
    });
  }

}