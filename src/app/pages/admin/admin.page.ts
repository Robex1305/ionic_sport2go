import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { ConversationService } from 'src/services/ConversationService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Conversation } from 'src/models/classes/Conversation';
import { Utilisateur } from 'src/models/classes/Utilisateur';
import { Groupe } from 'src/models/classes/Groupe';
import { UtilisateurService } from 'src/services/UtilisateurService';
import { SessionManager } from 'src/util/SessionManager';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  public constructor(public router: Router,
    public events: Events,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public popup: Popup,
    public utilisateurService:UtilisateurService,
    public sessionManager: SessionManager) { }

  ngOnInit() {
    this.utilisateurService.adminAccess().subscribe(() => {}, error => {
      if(error.status === 401){
        this.popup.showMessage("Accès interdit");
        this.sessionManager.destroy();
        this.router.navigateByUrl("/login");
      }
    });
  }

  public adminAdresses() {
    this.router.navigateByUrl("admin/adresses");
  }

  public adminConversations() {
    this.router.navigateByUrl("admin/conversations");
  }

  public adminEvenements() {
    this.router.navigateByUrl("admin/evenements");
  }

  public adminGeolocalisations() {
    this.router.navigateByUrl("admin/geolocalisations");
  }

  public adminGroupes() {
    this.router.navigateByUrl("admin/groupes");
  }

  public adminMessages() {
    this.router.navigateByUrl("admin/messages");
  }

  public adminSports() {
    this.router.navigateByUrl("admin/sports");
  }

  public adminUtilisateurs() {
    this.router.navigateByUrl("admin/utilisateurs");
  }

  public goBack() {
    this.router.navigateByUrl("/home");
  }
}
