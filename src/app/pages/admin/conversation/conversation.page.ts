import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { ConversationService } from 'src/services/ConversationService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Conversation } from 'src/models/classes/Conversation';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.page.html',
  styleUrls: ['./conversation.page.scss'],
})
export class ConversationPage implements OnInit {

  public listConversations: Array<Conversation>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public ConversationService: ConversationService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('conversation:create', (conversation) => {
      this.listConversations.push(conversation);
      this.trier();
    });

    this.events.subscribe('conversation:update', (conversation) => {
      this.listConversations = this.listConversations.filter(e => e.id !== conversation.id);
    });

    this.events.subscribe('conversation:delete', (conversation) => {
      this.listConversations = this.listConversations.filter(e => e.id !== conversation.id);
    });
  }

  ngOnInit() {
    this.popup.showLoader();
    this.ConversationService.readAll().subscribe(data => {
      this.listConversations = <Conversation[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des conversations, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listConversations.sort(function (a, b) {
      return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) { heures = "0" + heures; }
    if (minutes.length < 2) { minutes = "0" + minutes; }
    return heures + ":" + minutes;
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('conversations/new');
  }

  public addRecord(conversation: Conversation) {
    this.listConversations.push(conversation);
  }

  public updateRecord(conversation: Conversation) {
    this.listConversations.push(conversation);
  }

  public async delete(conversation: Conversation) {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer cette conversation?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.ConversationService.delete(conversation.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("La conversation a été supprimé");
            this.events.publish("conversation:delete", conversation);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression de la conversation")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
