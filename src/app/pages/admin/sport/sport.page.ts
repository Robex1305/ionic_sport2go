import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { SportService } from 'src/services/SportService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Sport } from 'src/models/classes/Sport';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-sport',
  templateUrl: './sport.page.html',
  styleUrls: ['./sport.page.scss'],
})
export class SportPage implements OnInit {

  public listSports: Array<Sport>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public SportService: SportService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('sport:create', (sport) => {
      this.listSports.push(sport);
      this.trier();
    });

    this.events.subscribe('sport:update', (sport) => {
      this.listSports = this.listSports.filter(e => e.id !== sport.id);
    });

    this.events.subscribe('sport:delete', (sport) => {
      this.listSports = this.listSports.filter(e => e.id !== sport.id);
    });

  }

  ngOnInit() {
    this.popup.showLoader();
    this.SportService.readAll().subscribe(data => {
      this.listSports = <Sport[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des sports, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listSports.sort(function (a, b) {
    return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) { heures = "0" + heures; }
    if (minutes.length < 2) { minutes = "0" + minutes; }
    return heures + ":" + minutes;
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('sports/new');
  }

  public addRecord(sport: Sport) {
    this.listSports.push(sport);
  }

  public updateRecord(sport: Sport) {
    this.listSports.push(sport);
  }

  public async delete(sport: Sport) {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer ce sport?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.SportService.delete(sport.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("La sport a été supprimé");
            this.events.publish("sport:delete", sport);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression du sport")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
