import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { UtilisateurService } from 'src/services/UtilisateurService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.page.html',
  styleUrls: ['./utilisateur.page.scss'],
})
export class UtilisateurPage implements OnInit {

  public listUtilisateurs: Array<Utilisateur>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public UtilisateurService: UtilisateurService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('utilisateur:create', (utilisateur) => {
      this.listUtilisateurs.push(utilisateur);
      this.trier();
    });

    this.events.subscribe('utilisateur:update', (utilisateur) => {
      this.listUtilisateurs = this.listUtilisateurs.filter(e => e.id !== utilisateur.id);
    });

    this.events.subscribe('utilisateur:delete', (utilisateur) => {
      this.listUtilisateurs = this.listUtilisateurs.filter(e => e.id !== utilisateur.id);
    });

  }

  ngOnInit() {
    this.popup.showLoader();
    this.UtilisateurService.readAll().subscribe(data => {
      this.listUtilisateurs = <Utilisateur[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des utilisateurs, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listUtilisateurs.sort(function (a, b) {
    return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) { heures = "0" + heures; }
    if (minutes.length < 2) { minutes = "0" + minutes; }
    return heures + ":" + minutes;
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('utilisateurs/new');
  }

  public addRecord(utilisateur: Utilisateur) {
    this.listUtilisateurs.push(utilisateur);
  }

  public updateRecord(utilisateur: Utilisateur) {
    this.listUtilisateurs.push(utilisateur);
  }

  public async delete(utilisateur: Utilisateur) {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer cet utilisateur?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.UtilisateurService.delete(utilisateur.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("L'utilisateur a été supprimé");
            this.events.publish("utilisateur:delete", utilisateur);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression de l'utilisateur")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
