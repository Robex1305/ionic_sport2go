import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { EvenementService } from 'src/services/EvenementService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Evenement } from 'src/models/classes/Evenement';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-evenement',
  templateUrl: './evenement.page.html',
  styleUrls: ['./evenement.page.scss'],
})
export class EvenementPage implements OnInit {

  public listEvenements: Array<Evenement>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public EvenementService: EvenementService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('evenement:create', (evenement) => {
      this.listEvenements.push(evenement);
      this.trier();
    });

    this.events.subscribe('evenement:update', (evenement) => {
      this.listEvenements = this.listEvenements.filter(e => e.id !== evenement.id);
    });

    this.events.subscribe('evenement:delete', (evenement) => {
      this.listEvenements = this.listEvenements.filter(e => e.id !== evenement.id);
    });

  }

  ngOnInit() {
    this.popup.showLoader();
    this.EvenementService.readAll().subscribe(data => {
      this.listEvenements = <Evenement[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des evenements, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listEvenements.sort(function (a, b) {
    return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) { heures = "0" + heures; }
    if (minutes.length < 2) { minutes = "0" + minutes; }
    return heures + ":" + minutes;
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('evenements/new');
  }

  public addRecord(evenement: Evenement) {
    this.listEvenements.push(evenement);
  }

  public updateRecord(evenement: Evenement) {
    this.listEvenements.push(evenement);
  }

  public async delete(evenement: Evenement) {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer cet évenement?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.EvenementService.delete(evenement.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("L'évenement a été supprimé");
            this.events.publish("evenement:delete", evenement);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression de l'évenement")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
