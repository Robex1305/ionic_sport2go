import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { MessageService } from 'src/services/MessageService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Message } from 'src/models/classes/Message';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit {

  public listMessages: Array<Message>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public MessageService: MessageService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('message:create', (message) => {
      this.listMessages.push(message);
      this.trier();
    });

    this.events.subscribe('message:update', (message) => {
      this.listMessages = this.listMessages.filter(e => e.id !== message.id);
    });

    this.events.subscribe('message:delete', (message) => {
      this.listMessages = this.listMessages.filter(e => e.id !== message.id);
    });

  }

  ngOnInit() {
    this.popup.showLoader();
    this.MessageService.readAll().subscribe(data => {
      this.listMessages = <Message[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des messages, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listMessages.sort(function (a, b) {
    return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('messages/new');
  }

  public addRecord(message: Message) {
    this.listMessages.push(message);
  }

  public updateRecord(message: Message) {
    this.listMessages.push(message);
  }

  public async delete(message: Message) {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer ce message?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.MessageService.delete(message.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("La message a été supprimé");
            this.events.publish("message:delete", message);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression du message")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
