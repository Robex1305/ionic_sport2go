import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { GeolocalisationService } from 'src/services/GeolocalisationService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Geolocalisation } from 'src/models/classes/Geolocalisation';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-geolocalisation',
  templateUrl: './geolocalisation.page.html',
  styleUrls: ['./geolocalisation.page.scss'],
})
export class GeolocalisationPage implements OnInit {

  public listGeolocalisations: Array<Geolocalisation>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public GeolocalisationService: GeolocalisationService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('geolocalisation:create', (geolocalisation) => {
      this.listGeolocalisations.push(geolocalisation);
      this.trier();
    });

    this.events.subscribe('geolocalisation:update', (geolocalisation) => {
      this.listGeolocalisations = this.listGeolocalisations.filter(e => e.id !== geolocalisation.id);
    });

    this.events.subscribe('geolocalisation:delete', (geolocalisation) => {
      this.listGeolocalisations = this.listGeolocalisations.filter(e => e.id !== geolocalisation.id);
    });

  }

  ngOnInit() {
    this.popup.showLoader();
    this.GeolocalisationService.readAll().subscribe(data => {
      this.listGeolocalisations = <Geolocalisation[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des geolocalisations, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listGeolocalisations.sort(function (a, b) {
    return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) { heures = "0" + heures; }
    if (minutes.length < 2) { minutes = "0" + minutes; }
    return heures + ":" + minutes;
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('geolocalisations/new');
  }

  public addRecord(geolocalisation: Geolocalisation) {
    this.listGeolocalisations.push(geolocalisation);
  }

  public updateRecord(geolocalisation: Geolocalisation) {
    this.listGeolocalisations.push(geolocalisation);
  }

  public async delete(geolocalisation: Geolocalisation) {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer cet geolocalisation?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.GeolocalisationService.delete(geolocalisation.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("La geolocalisation a été supprimé");
            this.events.publish("geolocalisation:delete", geolocalisation);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression de la geolocalisation")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
