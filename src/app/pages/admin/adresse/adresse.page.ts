import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { AdresseService } from 'src/services/AdresseService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Adresse } from 'src/models/classes/Adresse';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-adresse',
  templateUrl: './adresse.page.html',
  styleUrls: ['./adresse.page.scss'],
})
export class AdressePage implements OnInit {

  public listAdresses: Array<Adresse>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public AdresseService: AdresseService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('adresse:create', (adresse) => {
      this.listAdresses.push(adresse);
      this.trier();
    });

    this.events.subscribe('adresse:update', (adresse) => {
      this.listAdresses = this.listAdresses.filter(e => e.id !== adresse.id);
    });

    this.events.subscribe('adresse:delete', (adresse) => {
      this.listAdresses = this.listAdresses.filter(e => e.id !== adresse.id);
    });

  }

  ngOnInit() {
    this.popup.showLoader();
    this.AdresseService.readAll().subscribe(data => {
      this.listAdresses = <Adresse[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des adresses, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listAdresses.sort(function (a, b) {
      return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) { heures = "0" + heures; }
    if (minutes.length < 2) { minutes = "0" + minutes; }
    return heures + ":" + minutes;
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('adresses/new');
  }

  public addRecord(adresse: Adresse) {
    this.listAdresses.push(adresse);
  }

  public updateRecord(adresse: Adresse) {
    this.listAdresses.push(adresse);
  }

  public async delete(adresse: Adresse) {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer cet adresse?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.AdresseService.delete(adresse.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("L'adresse a été supprimé");
            this.events.publish("adresse:delete", adresse);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression de l'adresse")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
