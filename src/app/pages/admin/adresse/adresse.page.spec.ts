import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdressePage } from './adresse.page';

describe('AdressePage', () => {
  let component: AdressePage;
  let fixture: ComponentFixture<AdressePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdressePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdressePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
