import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController, AlertController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common';
import { GroupeService } from 'src/services/GroupeService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Groupe } from 'src/models/classes/Groupe';
import { Utilisateur } from 'src/models/classes/Utilisateur';

@Component({
  selector: 'app-groupe',
  templateUrl: './groupe.page.html',
  styleUrls: ['./groupe.page.scss'],
})
export class GroupePage implements OnInit {

  public listGroupes: Array<Groupe>;
  public loaderToShow: any;

  constructor(public router: Router,
    public events: Events,
    public GroupeService: GroupeService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public alertController: AlertController,
    public popup: Popup) {

    this.events.subscribe('groupe:create', (groupe) => {
      this.listGroupes.push(groupe);
      this.trier();
    });

    this.events.subscribe('groupe:update', (groupe) => {
      this.listGroupes = this.listGroupes.filter(e => e.id !== groupe.id);
    });

    this.events.subscribe('groupe:delete', (groupe) => {
      this.listGroupes = this.listGroupes.filter(e => e.id !== groupe.id);
    });

  }

  ngOnInit() {
    this.popup.showLoader();
    this.GroupeService.readAll().subscribe(data => {
      this.listGroupes = <Groupe[]>data;
      this.trier();
      this.popup.hideLoader();
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des groupes, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  public trier() {
    this.listGroupes.sort(function (a, b) {
    return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) { heures = "0" + heures; }
    if (minutes.length < 2) { minutes = "0" + minutes; }
    return heures + ":" + minutes;
  }

  public goBack() {
    this.router.navigateByUrl("/admin")
  }

  public formAddRecord() {
    this.router.navigateByUrl('groupes/new');
  }

  public addRecord(groupe: Groupe) {
    this.listGroupes.push(groupe);
  }

  public updateRecord(groupe: Groupe) {
    this.listGroupes.push(groupe);
  }

  public async delete(groupe: Groupe) {
    console.log(groupe);
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer ce groupe?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.GroupeService.delete(groupe.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("Le groupe a été supprimé");
            this.events.publish("groupe:delete", groupe);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression du groupe")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

}
