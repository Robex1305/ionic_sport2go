import {
  NgModule
} from '@angular/core';
import {
  PreloadAllModules,
  RouterModule,
  Routes
} from '@angular/router';
import {
  Guardian
} from 'src/util/Guardian';

const routes: Routes = [{
    path: '',
    canActivate: [Guardian],
    children: [{
        path: '',
        loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
      },
      {
        path: 'evenements/details/:evenementId',
        loadChildren: () => import('./tabs/tabevenements/page-details-evenement/page-details-evenement.module').then(m => m.PageDetailsEvenementPageModule)
      },
      {
        path: 'evenements/new',
        loadChildren: () => import('./tabs/tabevenements/page-nouvel-evenement/page-nouvel-evenement.module').then(m => m.PageNouvelEvenementPageModule),
      },
      { 
        path: 'conversations/details/:conversationId', 
        loadChildren: () => import('./tabs/tabmessages/page-details-conversation/page-details-conversation.module').then(m=> m.PageDetailsConversationPageModule), 
      },
      { 
        path: 'conversations/new', 
        loadChildren: () => import('./tabs/tabmessages/page-new-conversation/page-new-conversation.module').then(m=> m.PageNewConversationPageModule), 
      },
      { 
        path: 'groupes/details/:groupeId', 
        loadChildren: () => import('./tabs/tabsocial/page-details-groupe/page-details-groupe.module').then(m=> m.PageDetailsGroupePageModule), 
      },
      { 
        path: 'groupes/new', 
        loadChildren: () => import('./tabs/tabsocial/page-new-groupe/page-new-groupe.module').then(m=> m.PageNewGroupePageModule), 
      },
      { 
        path: 'admin/adresses', 
        loadChildren: () => import('./pages/admin/adresse/adresse.module').then(m=> m.AdressePageModule), 
      },
      { 
        path: 'admin/conversations', 
        loadChildren: () => import('./pages/admin/conversation/conversation.module').then(m=> m.ConversationPageModule), 
      },
      { 
        path: 'admin/evenements', 
        loadChildren: () => import('./pages/admin/evenement/evenement.module').then(m=> m.EvenementPageModule), 
      },
      { 
        path: 'admin/geolocalisations', 
        loadChildren: () => import('./pages/admin/geolocalisation/geolocalisation.module').then(m=> m.GeolocalisationPageModule), 
      },
      { 
        path: 'admin/messages', 
        loadChildren: () => import('./pages/admin/message/message.module').then(m=> m.MessagePageModule), 
      },
      { 
        path: 'admin/groupes', 
        loadChildren: () => import('./pages/admin/groupe/groupe.module').then(m=> m.GroupePageModule), 
      },
      { 
        path: 'admin/sports', 
        loadChildren: () => import('./pages/admin/sport/sport.module').then(m=> m.SportPageModule), 
      },
      { 
        path: 'admin/utilisateurs', 
        loadChildren: () => import('./pages/admin/utilisateur/utilisateur.module').then(m=> m.UtilisateurPageModule), 
      },
    ]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/connexion/page-log-in/page-log-in.module').then(m => m.PageLogInPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/connexion/page-register/page-register.module').then(m => m.PageRegisterPageModule)
  },
  { 
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminPageModule),
  },
  {
    path: "**", 
    redirectTo: "home"
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}