import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common'
import { GroupeService, } from 'src/services/GroupeService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Groupe } from 'src/models/classes/Groupe';
import { Utilisateur } from 'src/models/classes/Utilisateur';


@Component({
  selector: 'app-tabsocial',
  templateUrl: 'tabsocial.page.html',
  styleUrls: ['tabsocial.page.scss']
})
export class TabsocialPage {

  public listGroupes: Array<Groupe>;
  public loaderToShow: any;
  public loaded: boolean;

  //Ajout de listener a l'initialisation de la page
  public constructor(public routeur: Router,
    public events: Events,
    public GroupeService: GroupeService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public popup: Popup) {

    //Listener d'event "Nouvelle groupe crée"
    this.events.subscribe('groupe:create', (groupe) => {
      this.listGroupes.push(groupe);
      this.trier();
    });

    //Listener de suppression de groupe
    //TODO: a rework avec une vrai suppression en base
    //Listener d'event "Groupe supprimé"
    events.subscribe('groupe:delete', (groupe) => {
      this.listGroupes = this.listGroupes.filter(e => e.id !== groupe.id);
    });
  }

  ngOnInit() {
    
    this.loaded = false;
    this.popup.showLoader();
    this.GroupeService.readAll().subscribe(data => {
      this.listGroupes = <Groupe[]>data;
      this.trier();
      this.popup.hideLoader();
    this.loaded = true;
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des groupes, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  //Trie les groupes selon la date
  public trier() {
    this.listGroupes.sort(function (a, b) {
      return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) {
      heures = "0" + heures;
    }
    if (minutes.length < 2) {
      minutes = "0" + minutes;
    }

    return heures + ":" + minutes;
  }


  //Listener de clic: Navigation vers la page de détails de la groupe
  public onCardClick(groupe: Groupe) {
    this.routeur.navigateByUrl('groupes/details/' + groupe.id);
  }

  //Navigation vers la page de création d'une groupe
  public goToNewGroupe() {
    this.routeur.navigateByUrl('groupes/new');
  }

  //Méthode d'ajout d'une groupe. TODO: rework lors de la creation des CRUD
  public addEvent(groupe: Groupe) {
    this.listGroupes.push(groupe);
  }
}
