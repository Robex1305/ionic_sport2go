import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { NavController, ToastController, IonDatetime, Events, IonTextarea } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';
import { ChangeDetectorRef } from '@angular/core';
import { Popup } from 'src/util/Popup';
import { SessionManager } from 'src/util/SessionManager';
import { Groupe } from 'src/models/classes/Groupe';
import { error } from 'util';
import { GroupeService } from 'src/services/GroupeService';
import { Utilisateur } from 'src/models/classes/Utilisateur';
import { UtilisateurService } from 'src/services/UtilisateurService';

@Component({
  selector: 'app-page-new-groupe',
  templateUrl: './page-new-groupe.page.html',
  styleUrls: ['./page-new-groupe.page.scss'],
})
export class PageNewGroupePage implements OnInit {

  public groupe: Groupe;
  public loaded = false;
  toast: any;

  constructor(
    public groupeService: GroupeService,
    public utilisateurService: UtilisateurService,
    public router: Router,
    public events: Events,
    public toastController: ToastController,
    public httpClient: HttpClient,
    private changeRef: ChangeDetectorRef,
    public sessionManager: SessionManager,
    public popUp: Popup) { }


  /* Instanciation d'un nouvel évenement à l'initialisation de la page*/
  ngOnInit() {
    this.groupe = new Groupe();
    this.changeRef.detectChanges();
    this.loaded = true;
  }

  public createEvent() {
    //On vérifie la validité des informations rentrées dans l'évenement
    let check = this.validationGroupe();

    if (check) {
      let user = this.utilisateurService.getCurrentUser().subscribe(user => {
        this.popUp.showLoaderCustom("Création de la groupe...");
        this.groupe.admin = user;
        this.groupeService.create(this.groupe).subscribe(newEvent => {
          this.popUp.showMessage("Groupe \"" + this.groupe.nom + "\" créé avec succès");
          // console.log(newEvent)
          this.events.publish("groupe:create", <Groupe>newEvent);
          this.popUp.hideLoader();
          // this.goBack();
          this.router.navigateByUrl('groupes/details/' + newEvent);
        }, (error) => {
          console.log(error.status)
          this.popUp.showMessage("Une erreur est survenue lors de la création de la groupe, veuillez réessayer plus tard.")
          this.popUp.hideLoader()
          console.log(error);
        });
      })
    }
  }


  public validationGroupe() {
    let e = this.groupe;
    let messageToast: string;
    let check = true;

    if (e.nom === undefined || e.nom === '') {
      messageToast = "Le nom ne peux pas être vide"
      check = false
    } else if (e.nom.length < 5) {
      messageToast = "Le nom doit faire au moins 5 lettres"
      check = false
    } else if (e.description === undefined || e.description === '') {
      messageToast = "La description ne peux pas être vide"
      check = false
    } else if (e.description.length < 15) {
      messageToast = "Le description doit faire au moins 15 lettres"
      check = false
    }

    if (!check) {
      this.popUp.showMessage(messageToast);
    }
    return check;
  }

  public inputValidator(event: any) {
    //console.log(event.target.value);
    const pattern = /[^0-9]/;
    //let inputChar = String.fromCharCode(event.charCode)
    if (pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/, "");
      //invalid character, prevent input
    }
  }

  public goBack() {
    this.router.navigateByUrl("/groupes")
  }
}
