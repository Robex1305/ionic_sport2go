import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PageNewGroupePage } from './page-new-groupe.page';

const routes: Routes = [
  {
    path: '',
    component: PageNewGroupePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PageNewGroupePage]
})
export class PageNewGroupePageModule {}
