import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNewGroupePage } from './page-new-groupe.page';

describe('PageNewGroupePage', () => {
  let component: PageNewGroupePage;
  let fixture: ComponentFixture<PageNewGroupePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNewGroupePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNewGroupePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
