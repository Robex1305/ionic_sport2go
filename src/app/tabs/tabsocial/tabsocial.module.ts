import { IonicModule } from '@ionic/angular';
import {  Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabsocialPage } from './tabsocial.page';

const routes: Routes = [
  {
    path: '',
    component: TabsocialPage
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsocialPage]
})
export class TabsocialPageModule {}
