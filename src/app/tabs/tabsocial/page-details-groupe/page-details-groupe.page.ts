import { Component, OnInit } from '@angular/core';
import { Groupe } from 'src/models/classes/Groupe';
import { ActivatedRoute, Router } from '@angular/router';
import { TabsocialPage } from '../tabsocial.page';
import { Events, NavController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { GroupeService } from 'src/services/GroupeService';
import { EventEmitter, error } from 'protractor';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { SessionManager } from 'src/util/SessionManager';
import { Location } from '@angular/common';
import { UtilisateurService } from 'src/services/UtilisateurService';

@Component({
  selector: 'app-page-details-groupe',
  templateUrl: './page-details-groupe.page.html',
  styleUrls: ['./page-details-groupe.page.scss'],
})
export class PageDetailsGroupePage implements OnInit {


  public groupe: Groupe;
  public isMembre: boolean;
  public isAdmin: boolean;
  public loaded: boolean;
  public nombreMembres: number;

  public constructor(public loadingController: LoadingController,
    public groupeService: GroupeService,
    public utilisateurService: UtilisateurService,
    public sessionManager: SessionManager,
    private route: ActivatedRoute,
    private router: Router,
    public events: Events,
    public toastController: ToastController,
    public alertCtrl: AlertController,
    public popup: Popup,
    public dateUtil: DateUtil,
    public alertController: AlertController) { }

  ngOnInit() {
    this.loaded = false;
    this.popup.showLoader();
    this.route.paramMap.subscribe(params => {
      let stringId = params.get("groupeId");
      let id = parseInt(stringId);
      this.groupeService.read(id).subscribe((event) => {
        if (event !== null) {
          this.groupe = event;
          this.nombreMembres = this.groupe.membres.length;
          this.isMembre = false;
          // console.log(event);
          this.utilisateurService.getCurrentUser().subscribe(user => {
            this.groupe.membres.forEach(p => {
              if (p.id == user.id) {
                this.isMembre = true;
              }
            });

            this.isAdmin = false;
            if (this.groupe.admin.id === user.id) {
              this.isAdmin = true;
            }
            this.loaded = true;
          })
        } else {
          this.router.navigateByUrl("/../");
          this.popup.showMessage("Une erreur est survenue lors de la récupération du groupe")
        }
      }, (err) => {
        this.router.navigateByUrl("/../");
        this.popup.showMessage("Une erreur est survenue lors de la récupération du groupe")
      }, () => {
        this.popup.hideLoader();
      });
    });
  }

  public async delete() {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer ce groupe?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.groupeService.delete(this.groupe.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("Le groupe a été supprimé");
            this.events.publish("groupe:delete", this.groupe)
            this.router.navigateByUrl("/groupes");
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression du groupe")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }

  // public participer() {
  //   this.popup.showLoader();
  //   if (!this.isMembre) {
  //     let userId = this.utilisateurService.getCurrentUser().subscribe(user => {
  //       let eventId = this.groupe.id;
  //       this.groupeService.addMembre(user.id, eventId).subscribe(response => {
  //         this.popup.showMessage("Vous participez désormais à ce groupe");
  //         this.nombreMembres += 1;
  //         this.isMembre = true;
  //       }, error => {
  //         console.log(error);
  //         if (error.status == 401) {
  //           this.sessionManager.destroy();
  //         }
  //         else {
  //           this.popup.showMessage("Une erreur est survenue, veuillez réessayer plus tard");
  //         }
  //       }, () => {
  //         this.popup.hideLoader()
  //       });
  //     });
  //   } else {
  //     let userId = this.utilisateurService.getCurrentUser().subscribe(user => {
  //       let eventId = this.groupe.id;
  //       this.groupeService.removeMembre(user.id, eventId).subscribe(response => {
  //         this.nombreMembres -= 1;
  //         this.popup.showMessage("Vous ne participez plus à ce groupe")
  //         this.isMembre = false;
  //       }, error => {
  //         console.log(error);
  //         if (error.status == 401) {
  //           this.sessionManager.destroy();
  //         }
  //         else {
  //           this.popup.showMessage("Une erreur est survenue, veuillez réessayer plus tard");
  //         }
  //       }, () => {
  //         this.popup.hideLoader()
  //       });
  //     });
  //   }
  // }

}

