import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDetailsGroupePage } from './page-details-groupe.page';

describe('PageDetailsGroupePage', () => {
  let component: PageDetailsGroupePage;
  let fixture: ComponentFixture<PageDetailsGroupePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageDetailsGroupePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDetailsGroupePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
