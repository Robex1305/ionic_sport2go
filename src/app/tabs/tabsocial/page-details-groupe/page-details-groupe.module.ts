import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PageDetailsGroupePage } from './page-details-groupe.page';

const routes: Routes = [
  {
    path: '',
    component: PageDetailsGroupePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PageDetailsGroupePage]
})
export class PageDetailsGroupePageModule {}
