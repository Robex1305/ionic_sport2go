import { Component, OnInit } from '@angular/core';
import { Utilisateur } from 'src/models/classes/Utilisateur';
import { NavController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { EnumSexe } from 'src/models/enums/EnumSexe';
import { AuthenticationService } from 'src/services/AuthenticationService';
import { Popup } from 'src/util/Popup';
import { SessionManager } from 'src/util/SessionManager';
import { UtilisateurService } from 'src/services/UtilisateurService';

@Component({
  selector: 'app-tabhome',
  templateUrl: './tabhome.page.html',
  styleUrls: ['./tabhome.page.scss'],
})
export class TabhomePage implements OnInit {
  public user : Utilisateur;
  public hello : String; 
  public isAdmin : boolean;
  constructor(public utilisateurService:UtilisateurService, public navController: NavController, public router:Router, public sessionManager:SessionManager, public authService:AuthenticationService, public popUp:Popup) { }

  ngOnInit() {
    this.utilisateurService.adminAccess().subscribe(resp => {
      this.isAdmin = true;
    }, error => {
      if(error.status == 401){
        this.isAdmin = false;
      }
    });
  }

 


  public logout(){
    this.sessionManager.destroy();
    this.router.navigateByUrl("login");
    this.popUp.showMessage("Déconnecté")
  }

  public administration() {
    if(this.isAdmin){
    this.router.navigateByUrl("admin", {
      replaceUrl: true
    });
  }
  }

  public downloadAPK() {
    // this.router.navigateByUrl("admin", {
    //   replaceUrl: true
    // });
  }
}
