import {
  Component,
  OnInit
} from '@angular/core';
import {
  Evenement
} from 'src/models/classes/Evenement';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  TabevenementsPage
} from '../tabevenements.page';
import {
  Events,
  NavController,
  ToastController,
  AlertController,
  LoadingController
} from '@ionic/angular';
import {
  EvenementService
} from 'src/services/EvenementService';
import {
  EventEmitter, error
} from 'protractor';
import {
  DateUtil
} from 'src/util/DateUtil';
import {
  Popup
} from 'src/util/Popup';
import {
  SessionManager
} from 'src/util/SessionManager';
import {
  Location
} from '@angular/common';
import {
  UtilisateurService
} from 'src/services/UtilisateurService';

@Component({
  selector: 'app-page-details-evenement',
  templateUrl: './page-details-evenement.page.html',
  styleUrls: ['./page-details-evenement.page.scss'],
})
export class PageDetailsEvenementPage implements OnInit {

  public evenement: Evenement;
  public isParticipant: boolean;
  public isCreateur: boolean;
  public loaded: boolean;
  public nombreParticipants: number;

  public constructor(public loadingController: LoadingController,
    public evenementService: EvenementService,
    public utilisateurService: UtilisateurService,
    public sessionManager: SessionManager,
    private route: ActivatedRoute,
    private router: Router,
    public events: Events,
    public toastController: ToastController,
    public alertCtrl: AlertController,
    public popup: Popup,
    public dateUtil: DateUtil,
    public alertController: AlertController) {}

  ngOnInit() {
    this.loaded = false;
    this.popup.showLoader();
    this.route.paramMap.subscribe(params => {
      let stringId = params.get("evenementId");
        let id = parseInt(stringId);
        this.evenementService.read(id).subscribe((event) => {
          if (event !== null) {
            this.evenement = event;
            this.nombreParticipants = this.evenement.participants.length;
            this.isParticipant = false;
            this.utilisateurService.getCurrentUser().subscribe(user => {
              this.evenement.participants.forEach(p => {
                if (p.id == user.id) {
                  this.isParticipant = true;
                }
              });

              this.isCreateur = false;
              if (this.evenement.createur.id === user.id) {
                this.isCreateur = true;
              }
              this.loaded = true;
            })            
          } else {
            this.router.navigateByUrl("/../");
            this.popup.showMessage("Une erreur est survenue lors de la récupération de l'évenement")
          }
        }, (err) => {
          this.router.navigateByUrl("/../");
          this.popup.showMessage("Une erreur est survenue lors de la récupération de l'évenement")
        }, () => {
          this.popup.hideLoader();
        });
    });
  }

  public async delete() {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer cet évenement?",
      buttons: [{
          text: 'Non',
        },
        {
          text: 'Oui',
          handler: () => {
            this.popup.showLoader();
            this.evenementService.delete(this.evenement.id).subscribe(response => {
              this.popup.hideLoader();
              this.popup.showMessage("L'évenement a été supprimé");
              this.events.publish("evenement:delete", this.evenement)
              this.router.navigateByUrl("/evenements");
            }, error => {
              this.popup.showMessage("Une erreur est survenue lors de la suppression de l'évenement")
            },
            () => {
              this.popup.hideLoader();
            });
          }
        }
      ]
    })
    alert.present();
  }

  public participer() {
    this.popup.showLoader();

    if (!this.isParticipant) {
      let userId = this.utilisateurService.getCurrentUser().subscribe(user => {
        let eventId = this.evenement.id;
        this.evenementService.addParticipant(user.id, eventId).subscribe(response => {
          this.popup.showMessage("Vous participez désormais à cet évenement");
          this.nombreParticipants += 1;
          this.isParticipant = true;
        }, error => {
          console.log(error);
          if(error.status == 401){
            this.sessionManager.destroy();
          }
          else{
            this.popup.showMessage("Une erreur est survenue, veuillez réessayer plus tard");
          }
        }, () => {
          this.popup.hideLoader()
        });
      });
    } else {
      let userId = this.utilisateurService.getCurrentUser().subscribe(user => {
        let eventId = this.evenement.id;
        this.evenementService.removeParticipant(user.id, eventId).subscribe(response => {
          this.nombreParticipants -= 1;
          this.popup.showMessage("Vous ne participez plus à cet évenement")
          this.isParticipant = false;
        }, error => {
          console.log(error);
          if(error.status == 401){
            this.sessionManager.destroy();
          }
          else{
            this.popup.showMessage("Une erreur est survenue, veuillez réessayer plus tard");
          }
        }, () => {
          this.popup.hideLoader()
        });
      });
    }
  }

}