import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, Events, LoadingController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { DatePipe, getLocaleDateTimeFormat, getLocaleDayPeriods } from '@angular/common'
import { ConversationService, } from 'src/services/ConversationService';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { Conversation } from 'src/models/classes/Conversation';
import { Utilisateur } from 'src/models/classes/Utilisateur';
import { UtilisateurService, } from 'src/services/UtilisateurService';
import { Groupe } from 'src/models/classes/Groupe';
import { SessionManager } from 'src/util/SessionManager';

@Component({
  selector: 'app-tabmessages',
  templateUrl: 'tabmessages.page.html',
  styleUrls: ['tabmessages.page.scss']
})
export class TabmessagesPage {
  public listConversations: Array<Conversation>;
  public listConversationsGroupes: Array<Conversation>;
  public listConversationsContacts: Array<Conversation>;
  public listMesGroupes: Array<Conversation>;
  public listMesContacts: Array<Conversation>;
  public loaderToShow: any;
  public conversation: Conversation;
  public user: Utilisateur;
  public participant: Utilisateur;
  public loaded: boolean;

  //Ajout de listener a l'initialisation de la page
  public constructor(public routeur: Router,
    public events: Events,
    public ConversationService: ConversationService,
    public utilisateurService: UtilisateurService,
    public loadingController: LoadingController,
    public dateUtil: DateUtil,
    public sessionManager: SessionManager,
    public popup: Popup) {

    //Listener d'event "Nouvelle conversation crée"
    this.events.subscribe('conversation:create', (conversation) => {
      this.listConversations.push(conversation);
      this.trier();
    });

    //Listener de suppression de conversation
    //TODO: a rework avec une vrai suppression en base
    //Listener d'event "Conversation supprimé"
    events.subscribe('conversation:delete', (conversation) => {
      this.listConversations = this.listConversations.filter(e => e.id !== conversation.id);
    });
  }

  ngOnInit() {
    this.loaded = false;
    this.popup.showLoader();

    this.ConversationService.readAll().subscribe(data => {
      this.listConversations = <Conversation[]>data;
      this.user = this.sessionManager.getUser();
      this.trier();
      this.trierType();
      this.popup.hideLoader();
      this.loaded = true;
    }, error => {
      this.popup.showMessage("Une erreur est survenue lors de la récupération des conversations, veuillez réessayer plus tard");
      this.popup.hideLoader();
    });
  }

  //Trie les conversations selon la date
  public trier() {
    this.listConversations.sort(function (a, b) {
      return new Date(b.date_cre).getTime() - new Date(a.date_cre).getTime();
    })
  }

  public trierType() {
    this.listConversationsGroupes = [];
    this.listConversationsContacts = [];
    this.listMesGroupes = [];
    this.listMesContacts = [];

    console.log(this.user);

    this.listConversations.forEach(element => {
      if (element.typeGroupe === true) {
        this.listConversationsGroupes.push(element);
        // if (element.participants.length > 0) {
        //   element.participants.forEach(p => {
        //     if (p.id === this.user.id) { this.listMesGroupes.push(element); }
        //   });
        // }
      } else if (element.typeGroupe === false) {
        this.listConversationsContacts.push(element);
        // if (element.participants.length > 0) {
        //   element.participants.forEach(p => {
        //     if (p.id === this.user.id) { this.listMesContacts.push(element); }
        //   });
        // }
      }
    });
  }

  public formatDate(date: Date) {
    let d = new Date(date);
    let dLdS = d.toLocaleDateString();
    return dLdS;
  }

  public formatHeure(date: Date) {
    let d = new Date(date);
    let heures = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    if (heures.length < 2) {
      heures = "0" + heures;
    }
    if (minutes.length < 2) {
      minutes = "0" + minutes;
    }

    return heures + ":" + minutes;
  }


  //Listener de clic: Navigation vers la page de détails de la conversation
  public onCardClick(conversation: Conversation) {
    this.routeur.navigateByUrl('conversations/details/' + conversation.id);
  }

  //Navigation vers la page de création d'une conversation
  public goToNewConversation() {
    this.routeur.navigateByUrl('conversations/new');
  }

  //Méthode d'ajout d'une conversation. TODO: rework lors de la creation des CRUD
  public addEvent(conversation: Conversation) {
    this.listConversations.push(conversation);
  }
}
