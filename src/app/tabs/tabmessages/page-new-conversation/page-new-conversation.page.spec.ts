import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNewConversationPage } from './page-new-conversation.page';

describe('PageNewConversationPage', () => {
  let component: PageNewConversationPage;
  let fixture: ComponentFixture<PageNewConversationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNewConversationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNewConversationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
