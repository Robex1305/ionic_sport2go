import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { NavController, ToastController, IonDatetime, Events, IonTextarea } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';
import { ChangeDetectorRef } from '@angular/core';
import { Popup } from 'src/util/Popup';
import { SessionManager } from 'src/util/SessionManager';
import { Conversation } from 'src/models/classes/Conversation';
import { error } from 'util';
import { ConversationService } from 'src/services/ConversationService';
import { Utilisateur } from 'src/models/classes/Utilisateur';
import { UtilisateurService } from 'src/services/UtilisateurService';

@Component({
  selector: 'app-page-new-conversation',
  templateUrl: './page-new-conversation.page.html',
  styleUrls: ['./page-new-conversation.page.scss'],
})
export class PageNewConversationPage implements OnInit {
  public listUtilisateurs: Array<Utilisateur>;
  public participant: Utilisateur;
  public conversation: Conversation;
  public loaded = false;
  toast: any;

  constructor(
    public conversationService: ConversationService,
    public utilisateurService: UtilisateurService,
    public router: Router,
    public events: Events,
    public toastController: ToastController,
    public httpClient: HttpClient,
    private changeRef: ChangeDetectorRef,
    public sessionManager: SessionManager,
    public popUp: Popup) { }


  /* Retourne la date dans 2 ans a partir d'aujourd'hui qui servira de date maximum pour le datePicker
     afin d'éviter de se retrouver avec des événemenets le 27 Janvier 2599 */
  get maxDate() {
    let today = new Date();
    let year = (today.getFullYear() + 2).toString();
    return year;
  }

  /* Retourne l'année minimum qui servira de date minimum pour le datePicker, afin
     d'éviter de se retrouver avec des événemenets le 27 Janvier 1970 */
  get minDate() {
    let today = new Date();
    let year = today.getFullYear().toString();
    return year;
  }

  /* Instanciation d'un nouvel conversation à l'initialisation de la page*/
  ngOnInit() {
    this.popUp.showLoader();
    this.utilisateurService.readAll().subscribe(data => {
      this.listUtilisateurs = <Utilisateur[]>data;
      this.popUp.hideLoader();
    }, error => {
      this.popUp.showMessage("Une erreur est survenue lors de la récupération des événements, veuillez réessayer plus tard");
      this.popUp.hideLoader();
    });
    this.conversation = new Conversation();
    this.changeRef.detectChanges();
    this.loaded = true;
  }

  public createEvent() {
    //On vérifie la validité des informations rentrées dans la conversation
    let check = this.validationConversation();

    if (check) {
      let user = this.utilisateurService.getCurrentUser().subscribe(user => {
        this.popUp.showLoaderCustom("Création de la conversation...");
        this.conversation.addMembre(user);
        this.conversationService.create(this.conversation).subscribe(newEvent => {
          this.popUp.showMessage("Conversation \"" + this.conversation.titre + "\" créé avec succès");
          // console.log(newEvent)
          this.events.publish("conversation:create", <Conversation>newEvent);
          this.popUp.hideLoader();
          this.router.navigateByUrl('conversations/details/' + newEvent);
        }, (error) => {
          console.log(error.status)
          this.popUp.showMessage("Une erreur est survenue lors de la création de la conversation, veuillez réessayer plus tard.")
          this.popUp.hideLoader()
          console.log(error);
        });
      })
    }
  }


  public validationConversation() {
    let e = this.conversation;
    let messageToast: string;
    let check = true;
    this.conversation.addMembre(this.participant);

    if (e.titre === undefined || e.titre === '') {
      messageToast = "Le titre ne peux pas être vide"
      check = false
    } else if (e.titre.length < 5) {
      messageToast = "Le titre doit faire au moins 5 lettres"
      check = false
    } else if (this.participant == null && e.participants.length < 1) {
      messageToast = "Veuillez choisir un utilisateur"
      check = false
    }

    if (!check) {
      this.popUp.showMessage(messageToast);
    }
    return check;
  }

  public inputValidator(event: any) {
    //console.log(event.target.value);
    const pattern = /[^0-9]/;
    //let inputChar = String.fromCharCode(event.charCode)
    if (pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/, "");
      //invalid character, prevent input
    }
  }

  public goBack() {
    this.router.navigateByUrl("/conversations")
  }
}
