import { Conversation } from 'src/models/classes/Conversation';
import { Utilisateur } from 'src/models/classes/Utilisateur';
import { Message } from 'src/models/classes/Message';
import { TabmessagesPage } from '../tabmessages.page';
import { ConversationService } from 'src/services/ConversationService';
import { EventEmitter, error } from 'protractor';
import { DateUtil } from 'src/util/DateUtil';
import { Popup } from 'src/util/Popup';
import { SessionManager } from 'src/util/SessionManager';
import { Location } from '@angular/common';
import { UtilisateurService } from 'src/services/UtilisateurService';
import { MessageService } from 'src/services/MessageService';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NavigationExtras, Router, ActivatedRoute, } from '@angular/router';
import { NavController, ToastController, IonDatetime, Events, IonTextarea, AlertController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';
import { ChangeDetectorRef } from '@angular/core';


@Component({
  selector: 'app-page-details-conversation',
  templateUrl: './page-details-conversation.page.html',
  styleUrls: ['./page-details-conversation.page.scss'],
})
export class PageDetailsConversationPage implements OnInit {

  public conversation: Conversation;
  public isParticipant: boolean;
  public isCreateur: boolean;
  public loaded: boolean;
  public nombreParticipants: number;
  public listUtilisateurs: Array<Utilisateur>;
  public listMessages: Array<Message>;
  public message: Message;
  public user: Utilisateur;
  public toast: any;

  public constructor(public loadingController: LoadingController,
    public conversationService: ConversationService,
    public messageService: MessageService,
    public sessionManager: SessionManager,
    private route: ActivatedRoute,
    private router: Router,
    public events: Events,
    public toastController: ToastController,
    public popup: Popup,
    public dateUtil: DateUtil,
    public utilisateurService: UtilisateurService,
    public alertCtrl: AlertController,
    public alertController: AlertController,
    public httpClient: HttpClient,
    private changeRef: ChangeDetectorRef,
    public popUp: Popup) { }


  /* Instanciation d'un nouvel message à l'initialisation de la page*/
  ngOnInit() {
    this.loaded = false;
    this.popup.showLoader();
    this.conversation = new Conversation();
    this.route.paramMap.subscribe(params => {
      let stringId = params.get("conversationId");
      let id = parseInt(stringId);
      this.conversationService.read(id).subscribe((event) => {
        if (event !== null) {
          this.conversation = event;
          this.nombreParticipants = this.conversation.participants.length;
          this.isParticipant = false;
          // console.log(event);

          this.messageService.readAllByConversation(this.conversation.id).subscribe(data => {
            // console.log(this.conversation);
            this.conversation = this.conversation;
            this.listMessages = <Message[]>data;
            // console.log(this.listMessages);
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la récupération des messages, veuillez réessayer plus tard");
          });

          this.utilisateurService.getCurrentUser().subscribe(user => {
            this.conversation.participants.forEach(p => {
              if (p.id == user.id) {
                this.isParticipant = true;
              }
            });
          })

        } else {
          this.router.navigateByUrl("/../");
          this.popup.showMessage("Une erreur est survenue lors de la récupération de la conversation")
        }
      }, (err) => {
        this.router.navigateByUrl("/../");
        this.popup.showMessage("Une erreur est survenue lors de la récupération de la conversation")
      }, () => {
        this.popup.hideLoader();
      });
    });


    this.utilisateurService.getCurrentUser().subscribe(user => {
      this.user = user;
    });

    this.message = new Message();
    this.changeRef.detectChanges();
    this.loaded = true;
  }

  public createEvent() {
    //On vérifie la validité des informations rentrées dans le message
    let check = this.validationConversation();

    if (check) {
      let user = this.utilisateurService.getCurrentUser().subscribe(user => {
        this.popup.showLoaderCustom("Envoie du message...");
        this.message.emetteur = user;
        this.message.conversation = this.conversation;
        this.messageService.create(this.message).subscribe(newEvent => {
          this.popup.showMessage("Message envoyé avec succès");
          // console.log(newEvent);
          this.events.publish("message:create", <Message>newEvent);
          this.popup.hideLoader();
          this.ngOnInit();
        }, (error) => {
          console.log(error.status)
          this.popup.showMessage("Une erreur est survenue lors de l'envoie du message, veuillez réessayer plus tard.")
          this.popup.hideLoader();
          console.log(error);
        });
      })
    }
  }


  public validationConversation() {
    let e = this.message;
    let messageToast: string;
    let check = true;

    if (e.contenu === undefined || e.contenu === '') {
      messageToast = "Le message ne peux pas être vide"
      check = false
    } else if (e.contenu.length < 1) {
      messageToast = "Le message doit faire au moins 1 letcaractérestre"
      check = false
    } else if (e.contenu.length > 255) {
      messageToast = "Le message doit faire moins de 255 caractéres"
      check = false
    }

    if (!check) {
      this.popup.showMessage(messageToast);
    }
    return check;
  }

  public inputValidator(event: any) {
    //console.log(event.target.value);
    const pattern = /[^0-9]/;
    //let inputChar = String.fromCharCode(event.charCode)
    if (pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^0-9]/, "");
      //invalid character, prevent input
    }
  }

  public goBack() {
    this.router.navigateByUrl("/conversations")
  }


  public async delete() {
    const alert = await this.alertController.create({
      header: "Attention",
      message: "Êtes-vous sûr de vouloir supprimer cet conversation?",
      buttons: [{
        text: 'Non',
      },
      {
        text: 'Oui',
        handler: () => {
          this.popup.showLoader();
          this.conversationService.delete(this.conversation.id).subscribe(response => {
            this.popup.hideLoader();
            this.popup.showMessage("La conversation a été supprimé");
            this.events.publish("conversation:delete", this.conversation)
            this.router.navigateByUrl("/conversations");
          }, error => {
            this.popup.showMessage("Une erreur est survenue lors de la suppression de la conversation")
          },
            () => {
              this.popup.hideLoader();
            });
        }
      }
      ]
    })
    alert.present();
  }
}
