import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PageDetailsConversationPage } from './page-details-conversation.page';

const routes: Routes = [
  {
    path: '',
    component: PageDetailsConversationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PageDetailsConversationPage]
})
export class PageDetailsConversationPageModule {}
